function convert(cSharpClasses) {
    this.classRegex = /(public|private|internal)? (class|struct){1} (.+)/;
    this.propertyRegex = /(\t+)?(public|private|internal)? (.+){1}(<.+>)? (.+){1} \{(.+)}{1}\r?\n?/g;
    this.genericSymbolRegex = /(.+)<(.+)>/;
    this.derivedSymbolRegex = /(.+)( )+?:(.+)/;
    this.classIndex = 3;
    this.propertyIndex = 5;
    this.splitter = "#";

    this.match = function (str, regex, index) {
        var matched = regex.exec(str);
        if (!matched) return null;
        if (matched.length <= index) throw new Error(`Regex '${regex}' has ${matched.length} matches, but ${index} requested. Matches: ${matched.join(", ")}`);
        return matched[index];
    };

    this.matchAll = function (str, regex, index) {
        var match;
        var matches = [];

        do {
            match = this.match(str, regex, index);
            if (match) matches.push(match);
        } while (match);

        return matches;
    };

    ///strip the ViewModel or ApiModel suffix from the class
    this.stripProtectedSuffix = function (className) {
        if (!className) return className;
        var viewModelSuffix = /(.+)(ViewModel|ApiModel)$/i;
        var matches = viewModelSuffix.exec(className);
        if (!matches || matches.length !== 3) return className;
        return matches[1];
    };

    this.className = function (str) {
        var className = this.match(str, this.classRegex, this.classIndex);
        var isGeneric = !!this.genericSymbolRegex.exec(className);
        var isDerived = !!this.derivedSymbolRegex.exec(className);
        if (!isGeneric && !isDerived) return this.stripProtectedSuffix(className);
        //generic parser ignore derived types anyway, so no need to parse off the base/interface
        if (isGeneric) return this.stripProtectedSuffix(this.match(className, this.genericSymbolRegex, 1));
        if (isDerived) return this.stripProtectedSuffix(this.match(className, this.derivedSymbolRegex, 1));
    };

    this.camelCase = function (str) {
        str = str.replace("ID", "Id");
        return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
            if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
            return index == 0 ? match.toLowerCase() : match.toUpperCase();
        });
    };

    this.viewModelPropertyInitializer = function (propertyName, index, source) {
        var isLast = index === (source.length - 1);
        var camelCase = this.camelCase(propertyName);
        if (isLast) return `this.${camelCase} = null;`;
        return `this.${camelCase} = null;\r\n`;
    };

    this.apiModelPropertyInitializer = function (propertyName, index, source) {
        var isLast = index === (source.length - 1);
        if (isLast) return `this.${propertyName} = null;`;
        return `this.${propertyName} = null;\r\n`;
    };

    this.toViewModelConverter = function (pascalCasedPropertyNames, camelCasedPropertyNamed, className) {
        var cases = pascalCasedPropertyNames.map((p, i) => {
            return {
                pascalCase: p,
                camelCase: camelCasedPropertyNamed[i]
            };
        });
        var assignmentMapper = pair => `viewModel.${pair.camelCase} = apiModel.${pair.pascalCase};\r\n`;
        return `static fromApiModel(apiModel) {
        var viewModel = new ${className}ViewModel();
        if (!apiModel) return viewModel;
        ${cases.map(assignmentMapper).join("")}
        return viewModel;
        }`;
    };

    this.toApiModelConverter = function (pascalCasedPropertyNames, camelCasedPropertyNamed, className) {
        var cases = pascalCasedPropertyNames.map((p, i) => {
            return {
                pascalCase: p,
                camelCase: camelCasedPropertyNamed[i]
            };
        });
        var assignmentMapper = pair => `apiModel.${pair.pascalCase} = viewModel.${pair.camelCase};\r\n`;
        return `static fromViewModel(viewModel) {
        var apiModel = new ${className}ApiModel();
        if (!viewModel) return apiModel;
        ${cases.map(assignmentMapper).join("")}
        return apiModel;
        }`;
    };

    var classes = [];
    var transpiledClasses = "\r\n"; //it's easier to copy paste if there's a space at the start
    var split = cSharpClasses.split(this.splitter);
    for (var i = 0; i < split.length; i++) {
        var cSharpClass = split[i];
        var className = this.className(cSharpClass);
        var pascalCasedPropertyNames = this.matchAll(cSharpClass, this.propertyRegex, this.propertyIndex);
        var camelCasedPropertyNames = pascalCasedPropertyNames.map(this.camelCase);

        var transpiledClass = `class ${className}ViewModel {
        constructor () {
        ${pascalCasedPropertyNames.map(this.viewModelPropertyInitializer).join("")}
        }
        
        ${this.toViewModelConverter(pascalCasedPropertyNames, camelCasedPropertyNames, className)}
    }

    class ${className}ApiModel {
        constructor () {
        ${pascalCasedPropertyNames.map(this.apiModelPropertyInitializer).join("")}
        }
        
        ${this.toApiModelConverter(pascalCasedPropertyNames, camelCasedPropertyNames, className)}
    }\r\n`;

        transpiledClasses += transpiledClass;
    }

    return transpiledClasses;
};