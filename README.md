# README #

### What is this repository for? ###

This is a repository of javascript tools for those running a .NET Web API backend against an AngularJS 1 frontend

### How do I get set up? ###

* Pull
* Figure out which tool you want to use
* Run the tool in the console

### Tools ###

* \CSharpToJavaScript\convert - a conversion tool which takes a copy-pasted C# class and spits out a camel-cased ES6 class. 

### Contribution guidelines ###

Whatever, fix stuff if you want; this is more a place for me to stick things relevant to my job that might help other people.